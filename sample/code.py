'''
Created on Jan 29, 2013
@author: ^Clown^
Lesson Grabber
'''
from tkinter import *
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import messagebox
import re
import urllib.request
import urllib.error

def unit(*args):
    global rezult
    
    pat = regpat.get()
    url = site.get()
    html= siteurl(url)
    str =parseData(html,pat)
    t.insert(END, str)

def siteurl(url):
    
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', ' Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0')]
    try:
        response = opener.open(url)
        
        html = response.read().decode('utf-8')
        return html
    except  urllib.error.URLError as e:
        messagebox.showinfo(message='Can open url') 
        return ""
    
def clear(): 
   t.get("1.0",END)
   t.delete("1.0",END)
    
#need fix all
def parseData(st,pat):
    reg =re.compile(pat,re.M)
    m = reg.search(st)
    if(m is not None):
        return str(m.groups())
    else:
        return "cant search change pattern!"
    

window = Tk()
window.title("Lesson Grabber")
site= StringVar()
regpat = StringVar()
regpat.set(r"(\s+.*)")
site.set("http://one.lv")

mainframe = ttk.Frame(window, padding=(5, 5, 12, 0))

hello = {"text":"sample"}
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=15)

ttk.Label(mainframe,text="Site url").grid(column=2, row=1, sticky=(W, E))
field= ttk.Entry(mainframe, width=20, textvariable=site)
field.grid(column=2, row=2, sticky=(W, E))

ttk.Label(mainframe,text="Pattern Reg").grid(column=2, row=3, sticky=(W, E))
field2= ttk.Entry(mainframe, width=20, textvariable=regpat)
field2.grid(column=2, row=5, sticky=(W, E))

ttk.Button(mainframe, text="Grab",width=20, command=unit).grid(column=2, row=6, sticky=W)
ttk.Button(mainframe, text="Clear",width=20, command=clear).grid(column=2, row=8, sticky=W)
scrol = Scrollbar(mainframe)
t =Text(mainframe)
t.grid(column=2, row=7, sticky=W)
window.geometry("400x550")
window.mainloop()